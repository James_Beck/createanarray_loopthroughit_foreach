﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateAnArray_LoopThroughIt_ForEach
{
    class Program
    {
        static void Main(string[] args)
        {
            //Store of variables
            string[] colours = new string[5] { "red", "blue", "orange", "black", "white" };
            string name;

            //Explanation of program
            Console.WriteLine("Hi there user, what's your name?");
            name = Console.ReadLine();
            Console.WriteLine("Great. It's nice to meet you {0}. This program uses a foreach loop to present the information stored within an array.", name);
            Console.WriteLine("Press any key and let it do this when you're ready");
            Console.ReadKey();
            Console.WriteLine();
            foreach (string i in colours)
            {
                Console.WriteLine(i);
            }
        }
    }
}
